require 'chunky_png'
require 'rmagick'
require 'chunky_png/rmagick'

class Tile

  

  def initialize data
    @data = data
    @adj_hash = Hash.new(0)
  end

  def create_adjacency r
    r2 = r*r
    adj_dx = [];
    adj_dy = [];
    for dx in -r..r
      for dy in -r..r
        if(((dx*dx)+(dy*dy)) <= r2)
          adj_dx<<dx
          adj_dy<<dy
        end
      end
    end
    @adj_hash[r] = [adj_dx , adj_dy]
  end


  def process
    size = 512
    # ----------------------------------------------------------------
    png = ChunkyPNG::Image.new(size,size, ChunkyPNG::Color::TRANSPARENT)
    for x in 0...size
      for y in 0...size
        png[x,y] = ChunkyPNG::Color.from_hsl(0,0,0,0)
      end
    end
    # ----------------------------------------------------------------
    v_px    = [64,100,100]
    v_py    = [64,28,100]
    v_raios = [50,50,50]
    v_h     = [0,60,120]

    npoints = v_px.length

    i = 0;
    npoints.times do

      px = v_px[i]
      py = v_py[i]
      r = v_raios[i]
      mh = v_h[i]

      if not @adj_hash.has_key?(r)  
      	create_adjacency(r)  
      end
      
      adj_arr = @adj_hash[r]
      adj_dx = adj_arr[0]
      adj_dy = adj_arr[1]

      
      adj_dx.zip(adj_dy).each do
      |x,y|
      nx = px + x
      ny = py + y
      if ( nx >= 0 and nx < size and ny >= 0 and ny < size)
        	value = ChunkyPNG::Color.to_hsl(png[nx,ny],true)

        if value[3] != 0
          png[nx,ny] = ChunkyPNG::Color.from_hsl((mh+value[0])/2,0.9,0.5,127)
		else					
			png[nx,ny] = ChunkyPNG::Color.from_hsl(mh,0.9,0.5,127)
		end
	  end
	end

      i = i + 1
    end
    #------------------------------------------------------
	img = ChunkyPNG::RMagick.export(png)
	img = img.gaussian_blur(0, 10)
	img.write("filename2.png")

	return "urlToTile"
  end
end

a = Tile.new("a").process
